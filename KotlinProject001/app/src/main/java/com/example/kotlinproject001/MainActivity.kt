package com.example.kotlinproject001

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import android.content.Context.WIFI_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.net.wifi.WifiManager


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //ZONA DE BLUETOOTH

        val REQUEST_ENABLE_BT = 1

        val txt = findViewById(R.id.mensaje) as TextView

        val boton1 = findViewById(R.id.button1) as Button
        boton1.setOnClickListener { view ->

            val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            if (mBluetoothAdapter == null) {
                // Device does not support Bluetooth

            }
            if (!mBluetoothAdapter.isEnabled) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }
            val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0)
            startActivity(discoverableIntent)


            if (mBluetoothAdapter.isEnabled) {
                txt.setText("Bluetooth activated on discoverable mode")
            }

        }

        //ZONA DE VERIFICACIÓN


        val boton2 = findViewById(R.id.button2) as Button

        boton2.setOnClickListener { view ->

        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)
        val url = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList"

        // Request a string response from the provided URL.
        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                // Display the first 500 characters of the response string.
                txt.setText("Response is: ${response.substring(0, 500)}")

                val macAddress = android.provider.Settings.Secure.getString(getContentResolver(), "bluetooth_address")

                if (response.indexOf(macAddress)>=0){

                    txt.setText(" Your device exists in the attendance collection!")
                    }
                else{
                    txt.setText("We didn't find your device")
                }

            },
            Response.ErrorListener { txt.setText("That didn't work!") })


        // Add the request to the RequestQueue.
        queue.add(stringRequest)


    }
    }


}
